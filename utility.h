#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>


char *replaceString(const char *line, size_t len){
	char *and = "&amp;";
	char *gt = "&gt;";
	char *lt = "&lt;";
	size_t newSize = sizeof(char)*len*6;
	char *replacedString = (char *) malloc(newSize);
	
	size_t j =0;

	//Search for all occurenses for <, >, &.
	for(size_t i=0; i<len; i++){


		if(j > newSize-sizeof(char)*10){
			replacedString = (char *) realloc(replacedString, newSize+(sizeof(char)*10));
			newSize = newSize+(sizeof(char)*10);
		}

		if(line[i] == '<'){
			strcpy(&replacedString[j], lt);
			j += 4;

		} else if(line[i] == '>'){
			strcpy(&replacedString[j], gt);
			j += 4;

		} else if(line[i] == '&'){
			strcpy(&replacedString[j], and);
			j +=5;

		} else if(line[i] == '\0'){
			break;
		} else {
			replacedString[j] = line[i];
			j += 1;
		}
	}

	return replacedString;
}
