#include "../utility.h"
#include <stddef.h>
#include <stdint.h>

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {


    if(size < 1 || !data){
        return 0;
    }

     const char *cstring = (const char*) data;


    // Checks if the string is terminated.
    if(cstring[size -1] != 0 && cstring[size -1] != 10){
        return 0;
    }

    char *test = replaceString((const char *)data, size);
    free(test);



  return 0;
}
